from Dhenza7.linepy import *
from Dhenza7.akad.ttypes import Message
from Dhenza7.akad.ttypes import ContentType as Type
from Dhenza7.akad.ttypes import ChatRoomAnnouncementContents
from Dhenza7.akad.ttypes import ChatRoomAnnouncement
from datetime import datetime, timedelta
from time import sleep
from bs4 import BeautifulSoup
from humanfriendly import format_timespan, format_size, format_number, format_length
import time, random, multiprocessing, sys, json, codecs, threading, glob, re, string, os, requests, subprocess, six, ast, pytz, urllib, urllib3, urllib.parse, html5lib, wikipedia, atexit, timeit, pafy, youtube_dl, traceback
import pyimgflip
from gtts import gTTS
from googletrans import Translator
#Line Bot Kickers v.10
#Creator: DHENZA TBP
cl = LINE("")
cl.log("Auth Token : " + str(cl.authToken))
cl.log("Mid : " + str(cl.getProfile().mid))
print("SELF V.10 BY DHENZA")
oepoll = OEPoll(cl)
clID = cl.getProfile().mid
Me = [clID, "u80af617a9d40b29fed414864dc4627e6"]
set = {
  "autoJoinKick": False,
  "autoJoinTicket": True
}
def bot(op):
     try:
         if op.type == 0:
            print("END")
            return
         if op.type == 13:
           if set["autoJoinKick"] == True:
             cl.acceptGroupInvitation(op.param1)
             try:
                 G = cl.getGroup(op.param1)
                 targets = []
                 time.sleep(0.0001)
                 for member in G.members + G.invitee:
                      targets.append(member.mid)
                 for target in targets:
                     if target not in Me:
                       try:
    	                   cl.kickoutFromGroup(op.param1, [target])
                       except:
                           pass
                       try:
                           cl.cancelGroupInvitation(op.param1, [target])
                       except:
                           pass
             except:
             	pass
         if op.type == 25 or op.type == 26:
             msg = op.message
             text = str(msg.text)
             msg_id = msg.id
             receiver = msg.to
             sender = msg._from
             cmd = text.lower()
             time.sleep(0.0001)
             if msg.toType == 0 or msg.toType == 1 or msg.toType == 2:
               if sender != clID:
                 to = sender
               else:
                 to = receiver
             if msg.toType == 1:
               to = receiver
             if msg.toType == 2:
               to = receiver
             if msg.contentType == 0:
               if text is None:
                  return
               else:
                  if cmd == "*help":
                    if sender in Me:
                     cl.sendMessage(to, "🔴Help KICK SILENT*\n\n☠*kick\n☠*kickall\n☠*cancelall\n☠*kickcancelall\n☠*status\n☠*about\n\n☠*Settings\n☠autojoinkick:on|off\n☠autojointicket:on|off")
                  if cmd =="*status":
                   if sender in Me:
                     stts = "Status"
                     if set["autoJoinKick"] == True: stts += "\n-AutoJoinKick:On"
                     else: stts += "\n-AutoJoinKick:Off"
                     if set["autoJoinTicket"] == True: stts += "\n-AutoJoinTicket:On"
                     else: stts += "\n-AutoJoinTicket:Off"
                     stts += ""
                     cl.sendMessage(to, str(stts))
                  if cmd =="autojoinkick:on":
                    if sender in Me:
                    	set["autoJoinKick"] = True
                    cl.sendMessage(to, "AutoJoinKick Set On")
                  if cmd =="autojoinkick:off":
                    if sender in Me:
                    	set["autoJoinKick"] = False
                    cl.sendMessage(to, "AutoJoinKick Set Off")
                  if cmd =="autojointicket:on":
                    if sender in Me:
                    	set["autoJoinTicket"] = True
                    cl.sendMessage(to, "AutoJoinTicket Set On")
                  if cmd =="autojointicket:off":
                    if sender in Me:
                    	set["autoJoinTicket"] = False
                    cl.sendMessage(to, "AutoJoinTicket Set Off")
                  if cmd =="*speed":
                    if sender in Me:
                     start = time.time()
                     time.sleep(0.0001)
                     elapsed_time = time.time() - start
                     cl.sendMessage(to, "{}".format(str(elapsed_time)))
                  if cmd =="*about":
                     cl.sendMessage(to, "Bot v10\n\nCreator: TBP KICK V.10")
                  if cmd =="*kickall":
                   if sender in Me:
                     G = cl.getGroup(to)
                     targets = []
                     time.sleep(0.0001)
                     for member in G.members:
                         targets.append(member.mid)
                     for target in targets:
                         if target not in Me:
                           try:
    	                       cl.kickoutFromGroup(to, [target])
                           except:
                              pass
                  if cmd == "*cancelall":
                   if sender in Me:
                     G = cl.getGroup(to)
                     targets = []
                     time.sleep(0.0001)
                     for member in G.invitee:
                         targets.append(member.mid)
                     for target in targets:
                         if target not in Me:
                           try:
    	                       cl.cancelGroupInvitation(to, [target])
                           except:
                              pass
                  if cmd =="*kickcancelall":
                    if sender in Me:
                     G = cl.getGroup(to)
                     targets = []
                     time.sleep(0.0001)
                     for member in G.members + G.invitee:
                         targets.append(member.mid)
                     for target in targets:
                         if target not in Me:
                           try:
    	                       cl.kickoutFromGroup(to, [target])
                           except:
                              pass
                           try:
                               cl.cancelGroupInvitation(to, [target])
                           except:
                              pass
                  if cmd.startswith("*kick "):
                    if sender in Me:
                      if 'MENTION' in msg.contentMetadata.keys()!= None:
                         names = re.findall(r'@(\w+)', text)
                         mention = ast.literal_eval(msg.contentMetadata['MENTION'])
                         mentionees = mention['MENTIONEES']
                         targets = []
                         time.sleep(0.0001)
                         for mention in mentionees:
                             if mention["M"] not in targets:
                               targets.append(mention["M"])
                             for target in targets:
                             	if target not in Me:
                                   try:
                                        cl.kickoutFromGroup(to, [target])
                                   except:
                                        pass                                            
                  if "/ti/g/" in text.lower():
                    if set["autoJoinTicket"] == True:
                      link_re = re.compile("(?:line\:\/|line\.me\/R)\/ti\/g\/([a-zA-Z0-9_-]+)?")
                      links = link_re.findall(text)
                      n_links = []
                      for l in links:
                          if l not in n_links:
                            n_links.append(l)
                      for ticket_id in n_links:
                          findT = cl.findGroupByTicket(ticket_id)
                          if True:
                            cl.acceptGroupInvitationByTicket(findT.id, ticket_id)
     except Exception as e:
        cl.log("Error : " + str(e))
while True:
    try:
      ops=oepoll.singleTrace(count=50)
      if ops != None:
        for op in ops:
          bot(op)
          oepoll.setRevision(op.revision)
    except Exception as e:
        cl.log("Error : " + str(e))
